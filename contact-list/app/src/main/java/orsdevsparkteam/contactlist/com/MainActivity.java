package orsdevsparkteam.contactlist.com;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import orsdevsparkteam.contactlist.com.adapter.ContactAdapter;
import orsdevsparkteam.contactlist.com.model.ContactItem;


public class MainActivity extends ActionBarActivity {


    private static final String ACTION_CONTACT = "action_contact";

    private static final int NEW_CONTACT = 1;
    private static final int EDIT_CONTACT = 2;

    public static final String POSITION_MODEL_CONTACT = "position_model_contact";
    public static final String MODEL_CONTACT = "model_contact";


    private List<ContactItem> contactList;
    private ContactAdapter contactAdapter;
    private ListView contactListView;
    private GridView contactGridView;
    private ImageButton callContactButton;
    private ViewFlipper viewFlipper;
    private float lastX;
    private Button changeView ;
    // Search EditText
    EditText inputSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewflipper);

        bindViews();
        contactList = new ArrayList<ContactItem>();
        contactAdapter = new ContactAdapter(this, contactList);

        contactListView.setAdapter(contactAdapter);
        contactGridView.setAdapter(contactAdapter);
        Button btn = (Button) findViewById(R.id.buttonView);

        changeView = (Button)this.findViewById(R.id.buttonView);
        changeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFlipper.showPrevious();
            }
        });

        inputSearch = (EditText) findViewById(R.id.inputSearch);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                contactAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        // ASYNC CALL REQUIRED
        loadToDoList();
    }


    // Using the following method, we will handle all screen swaps.
    public boolean onTouchEvent(MotionEvent touchevent) {

        switch (touchevent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float currentX = touchevent.getX();

                // Handling left to right screen swap.
                if (lastX < currentX) {
                    // If there aren't any other children, just break.
                    if (viewFlipper.getDisplayedChild() == 0)
                    break;


                    // Display next screen.
                    viewFlipper.showNext();
                }

                // Handling right to left screen swap.
                if (lastX > currentX) {

                    // If there is a child (to the left), kust break.
                    if (viewFlipper.getDisplayedChild() == 1)
                    break;

                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }




    private void bindViews() {
        contactListView = (ListView)findViewById(R.id.listView);
        contactGridView = (GridView)findViewById(R.id.gridView);

        contactGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra(MODEL_CONTACT, contactList.get(position));
                intent.putExtra(POSITION_MODEL_CONTACT, position);
                startActivityForResult(intent, EDIT_CONTACT);
            }
        });

        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra(MODEL_CONTACT, contactList.get(position));
                intent.putExtra(POSITION_MODEL_CONTACT, position);
                startActivityForResult(intent, EDIT_CONTACT);
            }
        });

        contactListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(MainActivity.this).setTitle(getString(R.string.removeContactItem)).setMessage(getString(R.string.removeDoubleCheck) + contactList.get(position).getName() + " ?").setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, getString(R.string.removeContactItem) + contactList.get(position).getName(), Toast.LENGTH_SHORT).show();
                        deleteFromRealm(contactList.get(position));
                        contactList.remove(position);
                        contactAdapter.notifyDataSetChanged();
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, getString(R.string.userCancel), Toast.LENGTH_SHORT).show();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
                return true;
            }
        });

        contactGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(MainActivity.this).setTitle(getString(R.string.removeContactItem)).setMessage(getString(R.string.removeDoubleCheck) + contactList.get(position).getName() + " ?").setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, getString(R.string.removeContactItem) + contactList.get(position).getName(), Toast.LENGTH_SHORT).show();
                        deleteFromRealm(contactList.get(position));
                        contactList.remove(position);
                        contactAdapter.notifyDataSetChanged();
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, getString(R.string.userCancel), Toast.LENGTH_SHORT).show();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        //super.onResume();

       // ASYNC CALL REQUIRED
        //loadToDoList(contactList);
        saveTodoList(contactList);


        super.onResume();

    }

    @Override
    protected void onPause() {

        // ASYNC CALL REQUIRED
        saveTodoList(contactList);

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_contact) {
        //noinspection SimplifiableIfStatement
            Intent intent = new Intent(this, ContactActivity.class);
            startActivityForResult(intent, NEW_CONTACT);
            intent.putExtra(MODEL_CONTACT, new ContactItem());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_CONTACT || requestCode == EDIT_CONTACT) {
            if (resultCode == RESULT_OK) {
                int position = data.getIntExtra(POSITION_MODEL_CONTACT , -1);
                ContactItem contactItem = (ContactItem) data.getSerializableExtra(MODEL_CONTACT);
                if (position >= 0) {
                    Toast.makeText(this, "Update " + contactItem.getName() + " " + contactItem.getSurname()  + " as contact", Toast.LENGTH_SHORT).show();
                    contactList.set(position, contactItem);
                } else {
                    Toast.makeText(this, "Add " + contactItem.getName() + " " + contactItem.getSurname()  + " as contact", Toast.LENGTH_SHORT).show();
                    contactList.add(contactItem);
                }
                contactAdapter.notifyDataSetChanged();

            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "User Cancelled", Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

     private void loadToDoList() {
        if (contactList == null) {
            return;
        }
        contactList.clear();
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);

            for (ContactItem item : realm.allObjects(ContactItem.class)) {

                ContactItem itemToInsert = new ContactItem();
                itemToInsert.setId(item.getId());
                itemToInsert.setName(item.getName());
                itemToInsert.setSurname(item.getSurname());
                itemToInsert.setPhone(item.getPhone());
                itemToInsert.setAddress(item.getAddress());
                itemToInsert.setDescription(item.getDescription());
                itemToInsert.setEmail(item.getEmail());

                contactList.add(itemToInsert);
            }
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private void saveTodoList(List<ContactItem> list) {
        if (list == null) return;
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);

            // Copy the object to Realm. Any further changes must happen on realmUser
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(list);
            realm.commitTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private void deleteFromRealm(ContactItem contactItem) {
        if (contactItem == null) return;
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);
            realm.beginTransaction();

            //Query the ToDoItem and then remove from Realm

            ContactItem item = realm.where(ContactItem.class).equalTo("id", contactItem.getId()).findFirst();
            if (item != null) {
                item.removeFromRealm();
            }
            realm.commitTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void call(String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        try{
            Log.d("","Calling to: " + callIntent);
            startActivity(callIntent);
            Log.d("", "End Calling to: " + callIntent);
        }

        catch (android.content.ActivityNotFoundException ex){
            Log.d("","Exception: " + ex.getMessage());
            Toast.makeText(getApplicationContext(),"yourActivity is not founded", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendSMS(String phoneNumber) {
        Log.i("Send SMS", "");
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", phoneNumber);

        try {
            startActivity(smsIntent);
            Log.i("Finished sending SMS...", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Log.d("","Exception: " + ex.getMessage());
            Toast.makeText(MainActivity.this,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendEmail(String emailAccount) {
        Log.i("Send email", emailAccount);
        try {
            String uriText =
                    "mailto:" + emailAccount +
                            "?subject=" + Uri.encode("") +
                            "&body=" + Uri.encode("");

            Uri uri = Uri.parse(uriText);

            Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
            sendIntent.setData(uri);
            startActivity(Intent.createChooser(sendIntent, "Send email"));

            Log.i("MainActivity", "Finished sending email...");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Log.d("MainActivity","Exception: " + ex.getMessage());
            Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
