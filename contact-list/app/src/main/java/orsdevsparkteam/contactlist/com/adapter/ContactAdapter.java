package orsdevsparkteam.contactlist.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import orsdevsparkteam.contactlist.com.ContactActivity;
import orsdevsparkteam.contactlist.com.MainActivity;
import orsdevsparkteam.contactlist.com.R;
import orsdevsparkteam.contactlist.com.model.ContactItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sstjerne on 7/28/15.
 */

public class ContactAdapter extends BaseAdapter implements Filterable {

    private final Context context;
    private ValueFilter valueFilter;

    public static class ContactViewHolder{
        TextView contactText;
    }

    private List<ContactItem> contactList;
    private List<ContactItem> contactFilterList;

    public ContactAdapter(Context context, List<ContactItem> contactList){
        this.contactList = contactList;
        this.contactFilterList = contactList;
        this.context = context;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        if(contactList == null)
            return 0;

        return contactList.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public ContactItem getItem(int position) {
        return contactList.get(position);
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ContactViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.contact_item, parent, false);

            holder = new ContactViewHolder();
            holder.contactText = (TextView)convertView.findViewById(R.id.contactItemTextView);
            convertView.setTag(holder);
        }else{
            holder = (ContactViewHolder) convertView.getTag();
        }

        final ContactItem contactItem = getItem(position);
        if (contactItem != null) {
            holder.contactText.setText(contactItem.getName() + " " + contactItem.getSurname());
        }

        //Handle buttons and add onClickListeners
        ImageButton callButton = (ImageButton)convertView.findViewById(R.id.callButton);
        ImageButton smsButton = (ImageButton)convertView.findViewById(R.id.smsButton);
        ImageButton emailButton = (ImageButton)convertView.findViewById(R.id.emailButton);

        callButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                Log.d("", "Phone: " + contactItem.getPhone());
                if(context instanceof MainActivity){
                    ((MainActivity)context).call(contactItem.getPhone());
                }
                notifyDataSetChanged();
            }
        });

        smsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                Log.d("", "SMS to: " + contactItem.getPhone());
                if(context instanceof MainActivity){
                    ((MainActivity)context).sendSMS(contactItem.getPhone());
                }
                notifyDataSetChanged();
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                Log.d("Adapter:", "Email to: " + contactItem.getEmail());
                if(context instanceof MainActivity){
                    ((MainActivity)context).sendEmail(contactItem.getEmail());
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {

            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                contactList = (List<ContactItem>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();

                if (constraint != null && constraint.length() > 0){
                    ArrayList<ContactItem> filterList = new ArrayList<ContactItem>();
                    for (int i = 0; i < contactFilterList.size(); i++){
                        String dataNames = contactFilterList.get(i).getName() + " " + contactFilterList.get(i).getSurname();
                        if (dataNames.toLowerCase().contains(constraint.toString()))  {
                            ContactItem contact = new ContactItem();

                            contact.setAddress(contactFilterList.get(i).getAddress());
                            contact.setAvatarFullFileName(contactFilterList.get(i).getAvatarFullFileName());
                            contact.setAvatarThumbnailFileName(contactFilterList.get(i).getAvatarThumbnailFileName());
                            contact.setDescription(contactFilterList.get(i).getDescription());
                            contact.setEmail(contactFilterList.get(i).getEmail());
                            contact.setId(contactFilterList.get(i).getId());
                            contact.setName(contactFilterList.get(i).getName());
                            contact.setPhone(contactFilterList.get(i).getPhone());
                            contact.setSurname(contactFilterList.get(i).getSurname());
                            filterList.add(contact);
                        }
                    }

                    results.count = filterList.size();
                    results.values = filterList;
                }
                else {
                    results.count = contactFilterList.size();
                    results.values = contactFilterList;
                }

                Log.e("VALUES", results.values.toString());

                return results;
            }
    }
}
