package orsdevsparkteam.contactlist.com.model;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class ContactItem extends RealmObject implements Serializable {

	@PrimaryKey
	private long id = System.nanoTime();
	private String name = "";
    private String surname = "";
    private String email = "";
    private String phone = "";
    private String address = "";
    private String description = "";

    private String avatarFullFileName = "";
    private String avatarThumbnailFileName = "";


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvatarFullFileName() { return avatarFullFileName; }

    public void setAvatarFullFileName(String avatarFullFileName) { this.avatarFullFileName = avatarFullFileName; }

    public String getAvatarThumbnailFileName() { return avatarThumbnailFileName; }

    public void setAvatarThumbnailFileName(String avatarThumbnailFileName) { this.avatarThumbnailFileName = avatarThumbnailFileName; }
}