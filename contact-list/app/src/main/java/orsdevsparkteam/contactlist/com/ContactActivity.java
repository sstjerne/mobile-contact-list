package orsdevsparkteam.contactlist.com;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import orsdevsparkteam.contactlist.com.model.ContactItem;


public class ContactActivity extends ActionBarActivity {

    private Button addContactButton;
    private Button cancelContactButton;
    private EditText nameEditText;
    private EditText surnameEditText;

    private EditText addressEditText;
    private EditText phoneEditText;
    private EditText emailEditText;
    private EditText descriptionEditText;

    private ImageButton imageButton;

    private int position;
    private ContactItem contactItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Intent intent = getIntent();
        position = intent.getIntExtra(MainActivity.POSITION_MODEL_CONTACT , -1);
        contactItem = (ContactItem) intent.getSerializableExtra(MainActivity.MODEL_CONTACT);

        if (contactItem == null) {
            contactItem = new ContactItem();
        }

        bindViews();

    }

    private void bindViews() {
        addContactButton = (Button)findViewById(R.id.addContactbutton);
        nameEditText = (EditText)findViewById(R.id.nameEditText);
        surnameEditText = (EditText)findViewById(R.id.surnameEditText);
        addressEditText = (EditText)findViewById(R.id.addressEditText);
        phoneEditText = (EditText)findViewById(R.id.phoneEditText);
        emailEditText = (EditText)findViewById(R.id.emailEditText);
        descriptionEditText = (EditText)findViewById(R.id.descriptionEditText);

        addContactButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                contactItem.setName(nameEditText.getText().toString());
                contactItem.setSurname(surnameEditText.getText().toString());
                contactItem.setAddress(addressEditText.getText().toString());
                contactItem.setPhone(phoneEditText.getText().toString());
                contactItem.setEmail(emailEditText.getText().toString());
                contactItem.setDescription(descriptionEditText.getText().toString());
                returnIntent.putExtra(MainActivity.MODEL_CONTACT, contactItem);
                returnIntent.putExtra(MainActivity.POSITION_MODEL_CONTACT, position);

                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        cancelContactButton = (Button)findViewById(R.id.cancelContactButton);

        cancelContactButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        imageButton = (ImageButton) findViewById(R.id.imageButton1);

        if (contactItem.getAvatarFullFileName() != null) {
            imageButton.setImageURI(Uri.parse(contactItem.getAvatarFullFileName()));
        }

        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                selectImage();
            }
        });

        if (position >= 0 && contactItem != null) {
            setTitle("MODIFICAR");
            nameEditText.setText(contactItem.getName());
            surnameEditText.setText(contactItem.getSurname());
            addressEditText.setText(contactItem.getAddress());
            phoneEditText.setText(contactItem.getPhone());
            emailEditText.setText(contactItem.getEmail());
            descriptionEditText.setText(contactItem.getDescription());
            addContactButton.setText("UPDATE");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data){
        super.onActivityResult(reqCode, resCode, data);
        if (resCode == RESULT_OK) {
            if (reqCode == 1) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                    contactItem.setAvatarFullFileName(destination.toString());
                    imageButton.setImageBitmap(thumbnail);
                    imageButton.setVisibility(View.VISIBLE);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (reqCode == 2) {
                imageButton.setImageURI(data.getData());
                contactItem.setAvatarFullFileName(data.getDataString());
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



}
