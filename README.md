# CONTACT LIST #

The app should allow a user to manage a typical contact list, not the device’s contact list but one created solely for this app. The contacts must have their full name, address, phone number, email, and a description.

### User Stories ###
*As a user I want to see all my contacts in a list;*

*As a user I want to see one contact’s full data;*

*As a user I want to create and edit new contacts;*

*As a user I want to keep my contact list persistent between runs of the application;*

*As a user I want to attach a profile picture to a contact;*

*As a user I want to search contacts by name;*

*As a user I want to change the visualization of my contact list, changing between list and grid;*

*As a user I want to call a contact;*

*As a user I want to text a contact;*

*As a user I want to email a contact;*

*As a user I want to set a contact’s profile picture using the phone camera;*

*As a user I want to share a contact (For bonus points: the format should be VCF);*  